# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

# responsible for holding a value and holding a link
class LinkedListNode:
    def __init__(self, value=None, link=None):
        self.value= value
        self.link=link

# responsible for managing the nodes and linkes between nodes
class LinkedList:
    def __init__(self, head=None, tail=None):
        self.head = head
        self.tail = tail
        self._length = 0

    # users are able to read/see the length, but not set it
    @property
    def length(self):
        return self._length

    # traverse the list to where we want to insert/remove a node
    def traverse(self, idx):
        # check if list is empty or if idx is out of bounds
        if(self._length==0 or idx > (self._length - 1)):
            raise IndexError("idx out of range")

        # check in idx is an int
        if not isinstance(idx,int):
            raise TypeError("idx must be an int")
        
        i=0
        _current_node = self.head
        while i < idx:
            _current_node=_current_node.link
            i += 1
        return _current_node

    # if index is negative, 
    def get(self, idx):
        if idx < 0:
            idx = self._length + idx
        node = self.traverse(idx)
        return node.value

    # mutation 
    def insert(self, value, idx=None):
        new_node = LinkedListNode(value, link=None)

        # idx None (default)
        if idx is None:
            idx = self._length
        
        # case 1: empty list, inserting first node
        if self.head is None:
            self.head = new_node
            self.tail = new_node
            self._length += 1
            return None

        # case 2: insert at the end (non empty list)
        elif idx == self._length:
            self.tail.link = new_node
            self.tail = new_node
            self._length += 1

        # case 3: insert at the beginning
        elif idx == 0:
            new_node.link = self.head
            self.head = new_node
            self._length += 1

        # case 4: insert between two nodes
        else: 
            this_idx_node = self.traverse(idx-1)
            next_idx_node = this_idx_node.link

            new_node.link = next_idx_node
            this_idx_node.link = new_node
            self._length += 1
            return None

    def remove(self, idx=0):

        # if list is empty or idx is out of bounds
        if (self._length == 0 or idx > (self._length - 1)):
            raise IndexError("idx out of range")

        # if idx is not an int
        if not isinstance(idx, int):
            raise TypeError("idx must be an int")
        
        if idx==0:
            _val = self.head.value
            self.head = self.head.link
            self._length -= 1

            if self._length == 0:
                self.tail = None

            return _val

        previous_idx_node = self.traverse(idx-1)
        this_idx_node = previous_idx_node.link

        _val = this_idx_node.value

        next_idx_node = this_idx_node.link

        previous_idx_node.link = next_idx_node

        if next_idx_node is None:
            self.tail = previous_idx_node

        self._length -= 1
        return _val